#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"


using namespace cv;
using namespace std;

Mat ostateczny, wykryte_kontury, drawContTemp;
vector<vector<Point> > contours, najwiekszy;

vector<Vec4i> hierarchy;
int record;
int edgeThresh = 1;
int lowThreshold = 50;
int const max_lowThreshold = 100;
int ratio = 3;
int stateL = 0, prev1 = 0;
int kernel_size = 3;
int X, Y, i,j;

int main(int argc, char** argv)
{
	VideoCapture cap(0);
	if (!cap.isOpened())
	{
		cout << "Nie mog� odpali� kamerki :(" << endl;
		return -1;
	}


	Mat imgOryginalny, imgOryginalny1, frame;
	Mat imgGray;
	int Hmin = 143, Smin = 70, Vmin = 0;
	int Hmax = 179, Smax = 180, Vmax = 164;
	int iLastX = 0, iLastY = 0;

	namedWindow("Suwaczki Thresholdingu", CV_WINDOW_AUTOSIZE);
	createTrackbar("Hue_LowThreshold", "Control", &Hmin, 360);
	createTrackbar("Hue_HighThreshold", "Control", &Hmax, 360);
	createTrackbar("Sat_LowThreshold", "Control", &Smin, 360);
	createTrackbar("Sat_HighThreshold", "Control", &Smax, 360);
	createTrackbar("Val_LowThreshold", "Control", &Vmin, 360);
	createTrackbar("Val_HighThreshold", "Control", &Vmax, 360);


	namedWindow("Blur", CV_WINDOW_AUTOSIZE);
	namedWindow("Canny", CV_WINDOW_AUTOSIZE);
	namedWindow("Wykryte", CV_WINDOW_AUTOSIZE);

	while (true)
	{


		bool bSuccess = cap.read(frame);
		imgOryginalny1 = frame;


		if (!bSuccess)
		{
			cout << "Nie mog� wczyta� ramki z kamerki :(" << endl;
			break;
		}


		cvtColor(imgOryginalny1, imgOryginalny, CV_RGB2GRAY);
		threshold(imgOryginalny, imgGray, 2.0, 255, CV_THRESH_BINARY);

		erode(imgOryginalny, imgOryginalny, getStructuringElement(MORPH_ELLIPSE, Size(7, 7)));
		dilate(imgOryginalny, wykryte_kontury, getStructuringElement(MORPH_ELLIPSE, Size(7, 7))); //wyr�wnanie


		blur(wykryte_kontury, wykryte_kontury, Size(5, 5));
		imshow("Blur", wykryte_kontury);

		Canny(wykryte_kontury, wykryte_kontury, lowThreshold, lowThreshold*ratio, kernel_size); // kontury
		dilate(wykryte_kontury, wykryte_kontury, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
		imshow("Canny", wykryte_kontury);


		ostateczny.create(wykryte_kontury.size(), wykryte_kontury.type());
		ostateczny = Scalar::all(0); // ostateczny obrazek, do niego wrzucam



		imgOryginalny.copyTo(ostateczny, wykryte_kontury);
		dilate(ostateczny, ostateczny, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));


		//najwi�kszy kontur brany pod uwag�
		findContours(wykryte_kontury, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
		vector<Rect> boundRect(contours.size());
		int powierzchnia[400];
		Mat rysowanie = Mat::zeros(wykryte_kontury.size(), CV_8UC3);
		int najwi�kszykontur = 0;
		for (int i = 0; i < contours.size(); i++){

			approxPolyDP(contours[i], contours[i], 3, false);
			Scalar color = Scalar(5 * i, 50 + 3 * i, 250);
			boundRect[i] = boundingRect(Mat(contours[i]));
			powierzchnia[i] = boundRect[i].width*boundRect[i].height;
			if (i > 1)
			{
				if (powierzchnia[i] >= powierzchnia[najwi�kszykontur])
					najwi�kszykontur = i;
			}

		}

		drawContours(rysowanie, contours, najwi�kszykontur, Scalar(0, 0, 255), 2, 8, hierarchy, 0, Point());
		cvtColor(rysowanie, drawContTemp, COLOR_BGR2GRAY);

		// Moment zeby utrzyma� wykryt� r�ke
		Moments oMoments = moments(drawContTemp);
		double dM01 = oMoments.m01;
		double dM10 = oMoments.m10;
		double dArea = oMoments.m00;

		int posX = dM10 / dArea;
		int posY = dM01 / dArea;

		iLastX = posX;
		iLastY = posY;


		if (iLastX >= 0 && iLastY >= 0 && posX >= 0 && posY >= 0 )
		{
			line(rysowanie, Point(posX, posY), Point(iLastX, iLastY), Scalar(0, 255, 0), 2);
		}

		record++;
		X = posX - iLastX;
		Y = posY - iLastY;
		
		if (i == 0){
			i++;
		}else
		{
			if (X > 20 && Y < 20 && Y > -20)
			{
				putText(rysowanie, "Lewo", Point(100, 100), 2, 4, Scalar(255, 0, 0), 2, 8, true);
			}
			if (X< -20 && Y < 20 && Y > -20)
			{
				putText(rysowanie, "Prawo", Point(100, 100), 2, 4, Scalar(255, 0, 0), 2, 8, true);
			}
			if (Y > 20 && X < 20 && X >  20)
			{
				putText(rysowanie, "D�", Point(100, 100), 2, 4, Scalar(255, 0, 0), 2, 8, true);
			}
			if (Y < -20 && X < 20 && X > -20)
			{
				putText(rysowanie, "G�ra", Point(100, 100), 2, 4, Scalar(255, 0, 0), 2, 8, true);
			}
			iLastX = posX;
			iLastY = posY;
		}

		cout << powierzchnia[najwi�kszykontur] << "  ";
		add(rysowanie, frame, rysowanie);
		imshow("drawingtemp", rysowanie);

		if (waitKey(10) == 27)
		{
			cout << "no elo " << endl;
			break;
		}
	}

	return 0;
}