#include <sstream>
#include <string>
#include <iostream>
#include <opencv/cv.hpp>
#include <opencv2\highgui.hpp>


using namespace cv;
using namespace std;

void literki(){
	int A[3][2] = { { 1, 0 }, { 0, 0 }, { 0, 0} };
	int B[3][2] = { { 1, 0 }, { 1, 0 }, { 0, 0 } };
	int C[3][2] = { { 1, 1 }, { 0, 0 }, { 0, 0 } };
	int D[3][2] = { { 1, 1 }, { 0, 1 }, { 0, 0 } };
	int E[3][2] = { { 1, 0 }, { 0, 1 }, { 0, 0 } };
	int F[3][2] = { { 1, 1 }, { 1, 0 }, { 0, 0 } };
	int G[3][2] = { { 1, 1 }, { 1, 1 }, { 0, 0 } };
	int H[3][2] = { { 1, 0 }, { 1, 1 }, { 0, 0 } };
	int I[3][2] = { { 0, 1 }, { 1, 0 }, { 0, 0 } };
	int J[3][2] = { { 0, 1 }, { 1, 1 }, { 0, 0 } };
	int K[3][2] = { { 1, 0 }, { 0, 0 }, { 1, 0 } };
	int L[3][2] = { { 1, 0 }, { 1, 0 }, { 1, 0 } };
	int M[3][2] = { { 1, 1 }, { 0, 0 }, { 1, 0 } };
	int N[3][2] = { { 1, 1 }, { 0, 1 }, { 1, 0 } };
	int O[3][2] = { { 1, 0 }, { 0, 1 }, { 1, 0 } };
	int P[3][2] = { { 1, 1 }, { 1, 0 }, { 1, 0 } };
	int Q[3][2] = { { 1, 1 }, { 1, 1 }, { 1, 0 } };
	int R[3][2] = { { 1, 0 }, { 1, 1 }, { 1, 0 } };
	int S[3][2] = { { 0, 1 }, { 1, 0 }, { 1, 0 } };
	int T[3][2] = { { 0, 1 }, { 1, 1 }, { 1, 0 } };
	int U[3][2] = { { 1, 0 }, { 0, 0 }, { 1, 1 } };
	int V[3][2] = { { 1, 0 }, { 1, 0 }, { 1, 1 } };
	int W[3][2] = { { 0, 1 }, { 1, 1 }, { 0, 1 } };
	int X[3][2] = { { 1, 1 }, { 0, 0 }, { 1, 1 } };
	int Y[3][2] = { { 1, 1 }, { 0, 1 }, { 1, 1 } };
	int Z[3][2] = { { 1, 0 }, { 0, 1 }, { 1, 1 } };
	
	cout << A[1][1];
}

void wczytajobrazek()
{
	Mat img = imread("result.brf.png"); //read the image data in the file "MyPic.JPG" and store it in 'img'
	Mat img_gray;


	if (img.empty()) 
	{
		cout << "Error : Image cannot be loaded..!!" << endl;
		
	}

	cvtColor(img, img_gray, CV_BGR2GRAY);
	cvCircle(img_gray, img_gray, Size(9, 9), 2, 2);
	
	
	//Mat circles;
	//HoughCircles(img_gray, circles, CV_HOUGH_GRADIENT, 1, img_gray.rows, 200, 100, 0, 0);
//
//	for (size_t i = 0; i < circles.size(); i++)
//	{
//		//Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
//		//int radius = cvRound(circles[i][2]);
//		//// circle center
//		//circle(img, center,3, Scalar(0, 255, 0), -1, 8, 0);
//		////circle outline
//		//circle(img, center, radius, Scalar(0, 0, 255), 33, 8, 0);
//	
//		Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
//		int radius = cvRound(circles[i][2]);
//		// draw the green circle center
//		circle(img_gray, center, 3, Scalar(0, 255, 255), -1, 8, 0);
////		// draw the blue circle outline
////		circle(img_gray, center, radius, Scalar(0, 255, 0), 3, 8, 0);
//	}

 	namedWindow("okno", CV_WINDOW_AUTOSIZE);
	namedWindow("gray", CV_WINDOW_AUTOSIZE);
	namedWindow("circles", CV_WINDOW_AUTOSIZE);

	imshow("okno", img);
	imshow("gray", img_gray);
//	imshow("circles", circles);
	
	waitKey(0);
}
int main(int argc, char* argv[])
{
	int key = 0;
	VideoCapture capture;
	//capture.open(0);
	namedWindow("okno");
	int w = capture.get(CV_CAP_PROP_FRAME_WIDTH);
	int h = capture.get(CV_CAP_PROP_FRAME_HEIGHT);
	int fps = capture.get(CV_CAP_PROP_FRAME_COUNT);
	cout << "Width: " << w << "Height: " << h << "FPS: " << fps << endl;

	/*if (!capture.isOpened()){
	return -1;
}*/

	wczytajobrazek()
	/*while (1){
		Mat frame;
		capture >> frame;
		imshow("okno", frame);
		cout << "Width: " << w << "Height: " << h << "FPS: " << fps << endl;*/
	;
		/*if (waitKey(30) == 27 )
			break;
	}*/
	
	return 0;
}
